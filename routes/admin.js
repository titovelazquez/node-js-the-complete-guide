const path = require("path");
const express = require("express");
const rootDir = require("../utils/path");
const productsController = require("../controllers/products");
const router = express.Router();

// /admin/add-product => GET
router.get("/add-product", productsController.getAddProduct);

// /admin/product => POST
router.post("/add-product", productsController.postAddProduct);

module.exports = router;